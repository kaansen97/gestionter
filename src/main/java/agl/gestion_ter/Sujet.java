package agl.gestion_ter;

public class Sujet {
  //attributs
  private static int index;
  private String titre;
  private int id;

  //constructeurs
  public Sujet() {}

  public Sujet(String titre) {
    this.titre = titre;
    this.id = index;
    index++;
  }

  //getters and setters
  public String getTitre() {
    return this.titre;
  }
  public void setTitre(String titre) {
    this.titre = titre;
  }
  public int getId() {
    return this.id;
  }
  
  //méthode
  public String toString() {
    return this.titre + " (id:" + this.id + ")";
  }
}