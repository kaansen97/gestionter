package agl.gestion_ter;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Groupe {
  //attributs
  private int id;
  private static int idPartage;
  private String nom;
  private HashMap<Integer, Sujet> voeux;
  private boolean voeuFini;
  
  //constructeurs
  public Groupe() {}

  public Groupe(int id, int idPartage, String nom, Map<Integer, Sujet> voeux, boolean voeuFini) {
    this.id = id;
    setIdPartage(idPartage);
    this.nom = nom;
    this.voeux = (HashMap<Integer, Sujet>) voeux;
    this.voeuFini = voeuFini;
  }

  public Groupe(String nom) {
    this.id = idPartage;
    increaseId();
    this.nom = nom;
    this.voeux = new HashMap<>();
    this.voeuFini = false;
  }
  
  //getters and setters
  static void increaseId() {
	  idPartage += 1;
  }
  static void setIdPartage(int id) {
	  idPartage = id;
  }
  public boolean getVoeuFini() {
	  return this.voeuFini;
  }
  
  public void setNom(String nom) {
    this.nom = nom;
  }
  public String getNom() {
    return this.nom;
  }
  public int getId() {
    return this.id;
  }
  public void setVoeuFini(boolean voeuFini) {
    this.voeuFini = voeuFini;
  }
  public void setVoeux(Map<Integer, Sujet> hash) {
    this.voeux = (HashMap<Integer, Sujet>) hash;
  }
  
  //méthodes
  /**
   * Verifie si un sujet a déjà été ajouté aux voeux
   * @param Sujet suj
   * @return boolean true s'il est présent dans la liste
   */
  private boolean isSujetInVoeu(Sujet suj) {
    boolean inIt = false;
    for(Map.Entry<Integer,Sujet> entry : this.voeux.entrySet()) {
    	Sujet val = entry.getValue();
    	if(val == suj) {
    		inIt = true;
    	}
    }
    return inIt;
  }

  /**
   * Ajoute un voeu à la liste
   * @param Sujet suj
   * @param int ordre (entre 1 et 6, ordre de priorité) 
   * @return boolean true si le voeu a été correctement ajouté
   */
  public boolean addVoeu(int ordre, Sujet suj) throws CantAddVoeu{
    if(ordre > 0 && ordre < 6) {
      if(!this.isSujetInVoeu(suj)) {
        if(!this.voeux.keySet().contains(ordre)) {
          this.voeux.put(ordre, suj);
        } else {
        	throw new CantAddVoeu("L'ordre a déjà été rempli");
        }
      } else {
    	  throw new CantAddVoeu("Le sujet a déjà été choisi");
      }
    } else {
    	throw new CantAddVoeu("L'ordre du voeu doit être entre 1 et 5");
    }
    boolean complet = true;
    for(int i=1; i<6; i++) {
      if(this.voeux.get(i) == null) {
        complet = false;
      }
    }
    if(complet) {
      this.voeuFini = true;
    }
    return true;
  }

  public boolean isVoeuReady() {
    return this.voeuFini;
  }

  /**
   * toString
   * @return String instance bien formée
   */
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append(this.nom + " (id:" + this.id + ")\n"); 
    for(Map.Entry<Integer,Sujet> entry : this.voeux.entrySet()) {
    	Integer key = entry.getKey();
    	Sujet val = entry.getValue();
    	str.append("--" + key + ": " + val.toString() + "\n");
    }
    return str.toString();
  }

}